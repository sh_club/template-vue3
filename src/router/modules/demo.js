const AboutView = () => import('@/views/AboutView')

export const demoRoutes = [
  {
    path: '/about',
    name: 'about',
    component: AboutView
  }
]
