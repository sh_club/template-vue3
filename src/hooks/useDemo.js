/**
 * 项目所有单独功能的hooks
 */
export const useDemo = () => {
  const demoList = reactive([])
  return {
    demoList
  }
}
