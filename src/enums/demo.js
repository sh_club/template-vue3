import createEnum from './index'

export const DEMO = createEnum({
  ALL: ['0', 'all'],
  MAIN_STORE: ['1', 'main'],
  PUBLISH_TIME: ['2', 'time'],
})
