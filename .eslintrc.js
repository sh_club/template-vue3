module.exports = {
  extends: ['@mysteel', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto',
      },
    ],
    'no-undef': 'off',
    'no-console': 'off',
  },
}
